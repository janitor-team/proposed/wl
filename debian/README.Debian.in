@PACKAGE@ for Debian
------------------

The following setting enables you to run Wanderlust as a default mail
user agent of emacsen.

    (if (boundp 'mail-user-agent)
        (setq mail-user-agent 'wl-user-agent))
    (if (boundp 'read-mail-command)
        (setq read-mail-command 'wl))

To use SSL/TLS, install the ca-certificates package.  Wanderlust
uses Emacs's built-in GnuTLS support, and Emacs network security
is handled at a higher level via open-network-stream and the
Network Security Manager (NSM).  cf. gnutls.el and nsm.el

e.g.
    (setq network-security-level 'medium) ;; For the Network Security Manager (NSM)
    ;;(setq network-security-level 'high)
    (setq gnutls-verify-error nil) ;; The checks are performed by NSM
    ;;(setq gnutls-verify-error '(("\\.[0-9]+$") (".*" t)))
    (setq gnutls-min-prime-bits nil)
    ;;(setq gnutls-min-prime-bits 2048)
    (setq gnutls-algorithm-priority nil)
    ;;(setq gnutls-algorithm-priority "SECURE128:-VERS-SSL3.0:-VERS-TLS1.0:-VERS-TLS1.1")
    ;;(setq gnutls-algorithm-priority "NORMAL:-VERS-TLS1.3")
    (setq gnutls-log-level 0)
    ;;(setq gnutls-log-level 2) ;; For debugging, look in the *Messages* buffer

    (setq wl-auto-check-folder-name 'none)
    (setq wl-from "FULL NAME <USERNAME@example.com>")
    (setq wl-bcc wl-from)
    (setq wl-fcc "+backup")
    (setq wl-default-spec "%")
    (setq wl-default-folder "%inbox")
    ;;(setq wl-default-spec "+")
    ;;(setq wl-default-folder "+inbox")
    (setq wl-smtp-posting-server "smtp.example.com")
    (setq wl-smtp-posting-user "UESRNAME@example.com")
    (setq wl-smtp-authenticate-type "login")
    (setq wl-smtp-posting-port 465)
    (setq wl-smtp-connection-type 'ssl)
    ;;(setq wl-smtp-posting-port 25)
    ;;(setq wl-smtp-posting-port 587)
    ;;(setq wl-smtp-connection-type 'starttls)
    (setq elmo-imap4-default-server "imap.example.com")
    (setq elmo-imap4-default-user "UESRNAME@example.com")
    (setq elmo-imap4-default-authenticate-type 'clear)
    (setq elmo-imap4-default-port 993)
    (setq elmo-imap4-default-stream-type 'ssl)

    # ~/.folders
    %inbox
    +draft
    +queue
    +backup
    %:"UESRNAME@example.com"/clear@imap.example.com:993! /

If you'd like to avoid built-in GnuTLS support for some reason,
it is possible though not recommended to use tls.el, starttls.el, or
ssl.el.  To use the gnutls-cli command, install the gnutls-bin package.
To use the openssl command, install the openssl package.

e.g.
    (setq elmo-network-stream-type-alist
          '(;;("!" ssl nil elmo-open-gnutls-stream) ;; built-in GnuTLS
            ;;("!" ssl tls open-tls-stream) ;; emacs-27.1/lisp/obsolete/tls.el
            ("!" ssl ssl open-ssl-stream) ;; wanderlust/utils/ssl.el
            ("!!" starttls nil open-network-stream) ;; built-in GnuTLS
            ;;("!!" starttls starttls starttls-open-stream) ;; emacs-27.1/lisp/obsolete/starttls.el
            ("!socks" socks socks socks-open-network-stream)
            ("!direct" direct nil open-network-stream)
            ("!!socks" socks-ssl nil elmo-open-socks-ssl-stream)
            ("!!!socks" socks-starttls socks socks-open-network-stream)))
    (setq tls-checktrust t)
    (setq tls-program '("gnutls-cli --x509cafile /etc/ssl/certs/ca-certificates.crt --priority NORMAL -p %p %h"))
    (setq starttls-use-gnutls t)
    (setq starttls-extra-arguments '("--x509cafile" "/etc/ssl/certs/ca-certificates.crt" "--priority" "NORMAL"))
    (setq starttls-success "- Options:")
    (setq ssl-program-name "openssl")
    (setq ssl-program-arguments
          '("s_client" "-quiet"
            ;;"-cipher" "DEFAULT@SECLEVEL=2" "-no_ssl3" "-no_tls1" "-no_tls1_1"
            ;;"-cipher" "DEFAULT@SECLEVEL=1" "-no_tls1_3"
            "-CAfile" "/etc/ssl/certs/ca-certificates.crt"
            "-verify_return_error" "-verify" "8" "-verify_hostname" host
            "-port" service "-host" host))
    ;;(setq ssl-program-name "gnutls-cli")
    ;;(setq ssl-program-arguments '("--x509cafile" "/etc/ssl/certs/ca-certificates.crt" "--priority" "NORMAL" "-p" service host))

For more information, see the Info documents.

 -- Tatsuya Kinoshita <tats@debian.org>, Fri, 22 Jan 2021 19:57:14 +0900
